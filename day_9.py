import argparse
from typing import TextIO
from enum import Enum, auto
import functools


class Transform(Enum):
    UP = auto()
    DOWN = auto()
    LEFT = auto()
    RIGHT = auto()


ALL_TRANSFORMS = [Transform.UP, Transform.DOWN, Transform.LEFT, Transform.RIGHT]


class HeightMap:
    def __init__(self, text):
        self.hmap = [[int(x) for x in line] for line in text.split('\n')]
        self.height = len(self.hmap)
        self.width = len(self.hmap[0])

    def get_height(self, position):
        x, y = position
        return self.hmap[y][x] if 0 <= x < self.width and 0 <= y < self.height else None

    def transform(self, position, tf):
        x, y = position
        if tf == Transform.UP:
            if y > 0:
                return x, y - 1
            else:
                return None
        elif tf == Transform.DOWN:
            if y < self.height - 1:
                return x, y + 1
            else:
                return None
        elif tf == Transform.LEFT:
            if x > 0:
                return x - 1, y
            else:
                return None
        elif tf == Transform.RIGHT:
            if x < self.width - 1:
                return x + 1, y
            else:
                return None
        else:
            raise NotImplementedError(f'No transform: {tf}')

    def get_low_points(self):
        low_points = []
        for (y, row) in enumerate(self.hmap):
            for (x, cell) in enumerate(row):
                neighbors = [self.transform((x, y), t) for t in ALL_TRANSFORMS]
                neighbors = [self.get_height(n) for n in neighbors if n is not None]
                if cell < min(neighbors):
                    low_points.append((x, y))
        return low_points

    def get_basin_size(self, position):
        if self.get_height(position) == 9:
            return 0

        size = 0
        stack = [position]
        pushed = {position: True}

        while len(stack) != 0:
            # Pop the position off the stack and count it
            current_position = stack.pop()
            size += 1

            # Add all the neighbors that we haven't visited that aren't 9
            neighbors = [self.transform(current_position, t) for t in ALL_TRANSFORMS]
            neighbors = [n for n in neighbors if n is not None and self.get_height(n) != 9 and n not in pushed]
            for n in neighbors:
                stack.append(n)
                pushed[n] = True
        return size


def part_1(input_file: TextIO):
    height_map = HeightMap(input_file.read())
    low_points = height_map.get_low_points()
    low_points = [height_map.get_height(x) for x in low_points]
    risk_levels = [x + 1 for x in low_points]
    total_risk = sum(risk_levels)
    return total_risk


def part_2(input_file: TextIO):
    height_map = HeightMap(input_file.read())
    low_points = height_map.get_low_points()
    sizes = [height_map.get_basin_size(x) for x in low_points]
    sizes = sorted(sizes, reverse=True)
    score = functools.reduce(lambda a, b: a * b, sizes[:3])
    return score


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 9')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
